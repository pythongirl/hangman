#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>

void print_spaced_string(const char *string){
	printf("\n");
	for(int i = 0; i < strlen(string); i++){
		printf(" %c", string[i]);
	}
	printf("\n");
}

int main(){
	srand(time(NULL));

	char usedLetters[26] = {0};
	bool hasUnderscores, isCorrect, usedGuess;

	FILE *command_output = popen("sh -c \"cat /usr/share/dict/words | grep -P '^[\\x61-\\x7a]+$' | shuf -n1\"", "r");
	char word[64] = {0};
	fgets(word, 63, command_output);
	word[strlen(word) - 1] = '\0';
	fclose(command_output);

	int wordLength = strlen(word);

	char obscuredWord[wordLength + 1];
	obscuredWord[wordLength] = 0;
	for(int i = 0; i < wordLength; i++){
		obscuredWord[i] = '_';
	}

	int guessesLeft = 10;

	print_spaced_string(obscuredWord);

	while(guessesLeft > 0){
		char guess;

		printf("\n");
		printf("Guess a letter: ");
		int scanfResult = scanf(" %c", &guess);

		if(scanfResult == EOF){
			printf("Exiting.\n");
			return 0;
		} else if(scanfResult < 1){
			printf("Please enter a letter.\n");
			continue;
		}

		usedGuess = false;
		for(int i = 0; i < 26; i++){
			if(guess == usedLetters[i]){
				usedGuess = true;
				break;
			}
		}
		if(usedGuess){
			printf("You've already used that letter.\n");
			continue;
		}

		for(int i = 0; i < 26; i++){
			if(usedLetters[i] == 0){
				usedLetters[i] = guess;
				break;
			}
		}

		isCorrect = false;
		for(int i = 0; i < strlen(word); i++){
			if(guess == word[i]){
				obscuredWord[i] = word[i];
				isCorrect = true;
			}
		}

		if(!isCorrect){
			guessesLeft--;
		}

		print_spaced_string(obscuredWord);

		hasUnderscores = false;
		for(int i = 0; i < strlen(word); i++){
			if(obscuredWord[i] == '_'){
				hasUnderscores = true;
				break;
			}
		}

		if(!hasUnderscores){
			printf("\n");
			printf("\xf0\x9f\x8e\x89 You Win! \xf0\x9f\x8e\x89\n");
			printf("\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\xF0\x9F\x91\x8F\n");
			return 0;
		}

		printf("\n");

		if(!isCorrect) printf("You have %d wrong guesses left.\n", guessesLeft);

		if(guessesLeft == 0) break;

		printf("Letters you've used: ");
		for(int i = 0; i < 26; i++){
			if(usedLetters[i] == 0){
				break;
			}
			printf("%c ", usedLetters[i]);
		}
		printf("\n");
	}

	printf("\xf0\x9f\x98\xa2 You lost, the word was \"%s\"\n", word);

	return 0;
}
